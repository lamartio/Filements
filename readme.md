The first step will be to create a goal who is understandable by both the customer and the developer. This will ensure that both parties a striving for the same objective. That will be:

# A simple yet robust Android application for previewing images (and their data) from a online CSV.

Both parties agree this is enough information to start with and the next step will be planning the work for a 8 hour time-box. Since the objective is simple enough to accomplish it in one day, the objective is considered a task. Simply decomposing the task will suffice.

- Create a datamodel: Create an interface for simple access to the data
- Test the datamodel: Create a simple test for checking whether all the information is fetched properly
- Build the UI: Attach the datamodel to the UI. Initially the UI will be as simple as possible.

These steps will ensure the customer's satisfaction. When there is time left I will make a good a great app by adding the optional components.

- Add offline caching: Add level 2 caching and alternatively load the image from the web.
- Amplify the UI: Create a detail view by keeping the fragments autonomous. This will make it very easy to support MasterDetails flow.


## Notes
- Since it viewing a online file for Elements, I called the project Filements.
- Since time is of the essence and it is a small app, I don't care for amount of libraries (infamous MulitiDex problems).

## Features
- Visually appealing by using the Android and Material UI guidelins.
- Offline support. The app will automatically download all its necessities and store them on the device. 
- Pinch to zoom-in or to zoom-out.
- Swipe left or right to see the next or previous picture.
- Graceful data handling. Faulty text or imageURL's will not break the app.
- Get creative! Titles and descriptions supports HTML

## Docs
The application code is based on MV*. The * means Controller, ViewModel, Presenter or whatever binds your Model and View together. Though * is not implemented, because the app is small and thereby does not require full decompisition.

I defined a package called 'model' where all the application logic is placed. This functions as a interface for the rest of the application. Other components can use the logic by using the singleton and then URI-like methods: domain.com/pictures/all --> Data.getInstance().getPictures().getAll(). Do you see the similarity?

Endpoints (i.e. Picture or the list of pictures) implement an interface called Observable. Thanks to Observable; UI components can register an observer and invalidate whenever the Endpoint changes.  When the UI gets destroyed they unregister.
