package nl.lamarti.danny.filements.view;

import android.graphics.Bitmap;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import nl.lamarti.danny.filements.model.Picture;

/**
 * Created by DLamarti on 13-2-2016.
 */
public final class PictureViewHolder implements Runnable {

    public final TextView title;
    public final TextView description;
    public final View view;
    private final DisplayImageOptions displayOptions;
    private final ImageLoadingListener loadingListener = new SimpleImageLoadingListener() {

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            icon.setVisibility(View.INVISIBLE);
        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            icon.setVisibility(View.VISIBLE);
        }

    };
    public final ImageView icon;
    public Picture picture;

    public interface PictureClickListener {

        void onPictureClicked(Picture picture);

    }

    public PictureViewHolder(View view, DisplayImageOptions displayOptions, final PictureClickListener listener) {
        this.view = view;
        this.displayOptions = displayOptions;
        icon = (ImageView) view.findViewById(android.R.id.icon);
        title = (TextView) view.findViewById(android.R.id.text1);
        description = (TextView) view.findViewById(android.R.id.text2);

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (picture != null && !picture.isSelected())
                    picture.setSelected();

                if(picture != null && listener != null)
                    listener.onPictureClicked(picture);
            }
        });
    }

    public void bind(Picture picture) {
        unregister();
        this.picture = picture;
        register();
        run();
    }

    @Override
    public void run() {
        if (picture != null) {
            final Spanned title = Html.fromHtml(picture.title);
            final Spanned description = Html.fromHtml(picture.description);

            view.setActivated(picture.isSelected());
            this.title.setText(title);
            this.description.setText(description);

            ImageLoader.getInstance().cancelDisplayTask(icon);
            ImageLoader.getInstance().displayImage(picture.url, icon, displayOptions, loadingListener);
        } else {
            view.setActivated(false);
            title.setText("");
            description.setText("");
            icon.setVisibility(View.INVISIBLE);
        }
    }

    public void unregister() {
        if (this.picture != null)
            this.picture.unregister(this);
    }

    public void register() {
        if (this.picture != null)
            this.picture.register(this);
    }

}