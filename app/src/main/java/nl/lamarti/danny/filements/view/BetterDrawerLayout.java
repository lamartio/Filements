package nl.lamarti.danny.filements.view;

import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by DLamarti on 14-2-2016.
 *
 * Fixex bug: https://github.com/chrisbanes/PhotoView/issues/72
 */
public class BetterDrawerLayout extends DrawerLayout {

    public BetterDrawerLayout(Context context) {
        super(context);
    }

    public BetterDrawerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BetterDrawerLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent arg0) {
        try {
            return super.onInterceptTouchEvent(arg0);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
