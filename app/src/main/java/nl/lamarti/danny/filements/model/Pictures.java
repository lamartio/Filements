package nl.lamarti.danny.filements.model;

import android.content.Context;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import nl.lamarti.danny.filements.util.DownloadCallback;
import nl.lamarti.danny.filements.util.DownloadTask;
import nl.lamarti.danny.filements.util.Observable;
import nl.lamarti.danny.filements.util.ObservableArrayList;
import nl.lamarti.danny.filements.util.ObservableList;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;

/**
 * Created by DLamarti on 13-2-2016.
 */
public class Pictures {

    public final static String FILE_URL = "https://docs.google.com/spreadsheet/ccc?key=0Aqg9JQbnOwBwdEZFN2JKeldGZGFzUWVrNDBsczZxLUE&single=true&gid=0&output=csv";
    private final static String NAME = "filements_data";
    private final static String KEY = "pictures";

    final Data data;
    boolean isDownloading = false;
    final Observable selectedObservable = new Observable.Instance();
    final ObservableList<Picture> all = new ObservableArrayList<>();
    final DownloadCallback<List<Picture>> downloadCallback = new PicturesDownloadCallback(this);

    public Pictures(Data data) {
        this.data = data;
        loadQuietly();
    }

    public ObservableList<Picture> getAll() {
        return all;
    }

    public void download() {
        if (!isDownloading)
            DownloadTask.threadPoolExecute(downloadCallback);
    }

    public static CSVParser getParser() throws IOException {
        final InputStream input = getInputStream();
        final InputStreamReader reader = new InputStreamReader(input);
        final CSVFormat format = CSVFormat.DEFAULT.withIgnoreEmptyLines();
        final CSVParser parser = format.parse(reader);

        return parser;
    }

    public static InputStream getInputStream() throws IOException {
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Builder().url(FILE_URL).build();
        final Response response = client.newCall(request).execute();
        final InputStream input = response.body().byteStream();

        return input;
    }

    public static List<Picture> parse() throws IOException {
        final List<Picture> result = new ArrayList<>();
        final CSVParser parser = getParser();

        final Iterator<CSVRecord> iterator = parser.iterator();

        // skip the header
        iterator.next();

        while (iterator.hasNext()) {
            final CSVRecord record = iterator.next();
            final String title = record.get(0).trim();
            final String desc = record.get(1).trim();
            final String url = record.get(2).trim();
            final Picture picture = new Picture(title, desc, url);

            result.add(picture);
        }

        return result;
    }

    public void registerSelectedChangedObserver(Runnable observer) {
        selectedObservable.register(observer);
    }

    public void unregisterSelectedChangedObserver(Runnable observer) {
        selectedObservable.unregister(observer);
    }

    void setSelected(Picture newSelection) {
        if (newSelection != null) {
            for (Picture picture : all) {
                final boolean isSelection = picture == newSelection;

                if (picture.selected != isSelection) {
                    picture.selected = isSelection;

                    picture.notifyObservers();
                }
            }

            selectedObservable.notifyObservers();
        }
    }


    public int getSelectedIndex() {
        for (int i = 0; i < all.size(); i++) {
            if (all.get(i).selected)
                return i;
        }

        return -1;
    }

    public Picture getSelected() {
        for (Picture picture : all) {
            if (picture.selected)
                return picture;
        }

        return null;
    }

    private void loadQuietly() {
        try {
            load();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void load() {
        final String json = data.context
                .getSharedPreferences(NAME, Context.MODE_PRIVATE)
                .getString(KEY, null);

        if (json != null) {
            final Type type = new TypeToken<ArrayList<Picture>>() {
            }.getType();
            final ArrayList<Picture> pictures = new GsonBuilder()
                    .registerTypeAdapter(Picture.class, Picture.TYPE_ADAPTER)
                    .create()
                    .fromJson(json, type);

            if (pictures != null && !pictures.isEmpty()) {
                all.clear();
                all.addAll(pictures);
                all.notifyObservers();
            }
        }
    }

    void saveQuietly() {
        try {
            save();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void save() {
        final ObservableList<Picture> content = all;

        if (content != null && !content.isEmpty()) {
            final String json = new GsonBuilder()
                    .setPrettyPrinting()
                    .registerTypeAdapter(Picture.class, Picture.TYPE_ADAPTER)
                    .create()
                    .toJson(content);

            data.context
                    .getSharedPreferences(NAME, Context.MODE_PRIVATE)
                    .edit()
                    .putString(KEY, json)
                    .apply();
        }
    }
}
