package nl.lamarti.danny.filements.model;

import android.content.Context;

/**
 * Created by DLamarti on 13-2-2016.
 * <p/>
 * A singleton for accessing data. The class needs to be initialized first, so that it can access resources.
 */
public final class Data {

    private static Data instance;

    final Context context;
    final Pictures pictures;

    public final static Data initInstance(Context context) {
        return instance = new Data(context);
    }

    public final static Data getInstance() {
        if (instance == null)
            throw new IllegalStateException("Needs to be initialized first. Use the Data.initInstance() static method first!");

        return instance;
    }

    private Data(Context context) {
        this.context = context;
        this.pictures = new Pictures(this);
    }

    public Pictures getPictures() {
        return pictures;
    }

}
