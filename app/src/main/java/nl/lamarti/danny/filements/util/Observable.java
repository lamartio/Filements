package nl.lamarti.danny.filements.util;

import java.util.ArrayList;
import java.util.List;

public interface Observable {

    void register(Runnable observer);

    void unregister(Runnable observer);

    void notifyObservers();

    class Instance implements Observable {

        private final transient List<Runnable> observers = new ArrayList<Runnable>();

        public Instance() {
        }

        @Override
        public void notifyObservers() {
            for (Runnable observer : observers) {
                if (observer != null)
                    observer.run();
            }
        }

        public int size() {
            return observers.size();
        }

        @Override
        public void register(Runnable observer) {
            if (observer != null)
                observers.add(observer);
        }

        @Override
        public void unregister(Runnable observer) {
            observers.remove(observer);
        }

    }

}
