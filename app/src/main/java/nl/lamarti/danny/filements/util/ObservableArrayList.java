package nl.lamarti.danny.filements.util;

import java.util.ArrayList;

public class ObservableArrayList<E> extends ArrayList<E> implements ObservableList<E> {

	private static final long serialVersionUID = -6462555192617202242L;
	private final transient Observable.Instance observable = new Observable.Instance();

	@Override
	public void register(Runnable observer) {
		observable.register(observer);
	}

	@Override
	public void unregister(Runnable observer) {
		observable.unregister(observer);
	}

	@Override
	public void notifyObservers() {
		observable.notifyObservers();
	}

}