package nl.lamarti.danny.filements.model;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import nl.lamarti.danny.filements.util.Observable;

/**
 * Created by DLamarti on 13-2-2016.
 */
public class Picture implements Observable {


    public static final TypeAdapter<Picture> TYPE_ADAPTER = new TypeAdapter<Picture>() {
        @Override
        public void write(JsonWriter out, Picture value) throws IOException {
            out.beginObject();
            out.name("title").value(value.title);
            out.name("description").value(value.description);
            out.name("url").value(value.url);
            out.name("selected").value(value.selected);
            out.endObject();
        }

        @Override
        public Picture read(JsonReader in) throws IOException {
            in.beginObject();

            in.nextName();
            final String title = in.nextString();
            in.nextName();
            final String description = in.nextString();
            in.nextName();
            final String url = in.nextString();
            in.nextName();
            final boolean selected = in.nextBoolean();
            final Picture picture = new Picture(title, description, url, selected);

            in.endObject();
            return picture;
        }
    };
    public final String title;
    public final String description;
    public final String url;
    boolean selected = false;

    private final Observable observable = new Observable.Instance();

    public Picture(String title, String description, String url) {
        this(title, description, url, false);
    }

    public Picture(String title, String description, String url, boolean selected) {
        this.title = title;
        this.description = description;
        this.url = url;
        this.selected = selected;
    }

    public void setSelected() {
        Data.getInstance().pictures.setSelected(this);
    }

    public boolean isSelected() {
        return selected;
    }

    @Override
    public void register(Runnable observer) {
        observable.register(observer);
    }

    @Override
    public void unregister(Runnable observer) {
        observable.unregister(observer);
    }

    @Override
    public void notifyObservers() {
        observable.notifyObservers();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Picture) {
            final Picture p = (Picture) o;

            return equals(p.title, title)
                    && equals(p.description, description)
                    && equals(p.url, url);
        }

        return false;
    }

    public boolean hasEqualData(Picture picture) {
        return equals(picture.title, title)
                && equals(picture.description, description)
                && equals(picture.url, url);
    }

    private boolean equals(String a, String b) {
        return (a == null) ? (b == null) : a.equals(b);
    }
}
