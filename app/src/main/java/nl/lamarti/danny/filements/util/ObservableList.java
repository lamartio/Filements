package nl.lamarti.danny.filements.util;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public interface ObservableList<E> extends List<E>, Observable {

	class UnmodifiableInstance<E> implements ObservableList<E>{

		public final ObservableList<E> content;
		
		public UnmodifiableInstance() {
			this(new ObservableArrayList<E>());
		}

		public UnmodifiableInstance(ObservableList<E> content) {
			this.content = content;
		}

		@Override
		public boolean add(E arg0) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void add(int arg0, E arg1) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean addAll(Collection<? extends E> arg0) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean addAll(int arg0, Collection<? extends E> arg1) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void clear() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean contains(Object arg0) {
			return content.contains(arg0);
		}

		@Override
		public boolean containsAll(Collection<?> arg0) {
			return content.containsAll(arg0);
		}

		@Override
		public E get(int arg0) {
			return content.get(arg0);
		}

		@Override
		public int indexOf(Object arg0) {
			return content.indexOf(arg0);
		}

		@Override
		public boolean isEmpty() {
			return content.isEmpty();
		}

		@Override
		public Iterator<E> iterator() {
			return content.iterator();
		}

		@Override
		public int lastIndexOf(Object arg0) {
			return content.lastIndexOf(arg0);
		}

		@Override
		public ListIterator<E> listIterator() {
			return new UnmodifiableListIterator<E>(content.listIterator());
		}

		@Override
		public ListIterator<E> listIterator(int index) {
			return new UnmodifiableListIterator<E>(content.listIterator(index));
		}

		@Override
		public boolean remove(Object arg0) {
			throw new UnsupportedOperationException();
		}

		@Override
		public E remove(int arg0) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean removeAll(Collection<?> arg0) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean retainAll(Collection<?> arg0) {
			throw new UnsupportedOperationException();
		}

		@Override
		public E set(int arg0, E arg1) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int size() {
			return content.size();
		}

		@Override
		public List<E> subList(int arg0, int arg1) {
			return Collections.unmodifiableList(content.subList(arg0, arg1));
		}

		@Override
		public Object[] toArray() {
			return content.toArray();
		}

		@Override
		public <T> T[] toArray(T[] arg0) {
			return content.toArray(arg0);
		}

		@Override
		public void register(Runnable observer) {
			content.register(observer);
		}

		@Override
		public void unregister(Runnable observer) {
			content.unregister(observer);
		}

		@Override
		public void notifyObservers() {
			content.notifyObservers();
		}

	}
	
	class UnmodifiableListIterator<E> implements ListIterator<E> {
		
		private final ListIterator<E> it;

		private UnmodifiableListIterator(ListIterator<E> it) {
			this.it = it;
		}

		@Override
		public void add(E e) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean hasNext() {
			return it.hasNext();
		}

		@Override
		public boolean hasPrevious() {
			return it.hasPrevious();
		}

		@Override
		public E next() {
			return it.next();
		}

		@Override
		public int nextIndex() {
			return it.nextIndex();
		}

		@Override
		public E previous() {
			return it.previous();
		}

		@Override
		public int previousIndex() {
			return it.previousIndex();
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void set(E e) {
			throw new UnsupportedOperationException();
		}
		
	}

}
