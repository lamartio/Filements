package nl.lamarti.danny.filements.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.util.ArrayList;

import nl.lamarti.danny.filements.R;
import nl.lamarti.danny.filements.model.Picture;
import nl.lamarti.danny.filements.view.PictureViewHolder.PictureClickListener;

/**
 * Created by DLamarti on 13-2-2016.
 */
public final class PicturesAdapter extends ArrayAdapter<Picture> {

    private final LayoutInflater inflater;
    private final DisplayImageOptions displayOptions;
    private final PictureClickListener listener;

    public PicturesAdapter(Context context, LayoutInflater inflater, DisplayImageOptions displayOptions, PictureClickListener listener) {
        super(context, 0, new ArrayList<Picture>());
        this.inflater = inflater;
        this.displayOptions = displayOptions;
        this.listener = listener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Picture picture = getItem(position);
        final PictureViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.picture_list_item, parent, false);
            viewHolder = new PictureViewHolder(convertView, displayOptions, listener);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (PictureViewHolder) convertView.getTag();
        }

        viewHolder.bind(picture);
        return convertView;
    }

}