package nl.lamarti.danny.filements.util;

/**
 * Created by DLamarti on 13-2-2016.
 *
 * This utility simplifies the download process by giving events convenient names.
 */

import android.os.AsyncTask;
import android.support.v4.util.Pair;

public class DownloadTask<T> extends AsyncTask<Object, Float, Pair<Exception, T>> implements DownloadCallback<T> {

    private final DownloadCallback<T> callback;

    public DownloadTask(DownloadCallback<T> callback) {
        this.callback = callback != null ? callback : DownloadCallback.EMPTY_INSTANCE;
    }

    public static <T> void serialExecute(DownloadCallback<T> callback) {
        new DownloadTask(callback).executeOnExecutor(DownloadTask.SERIAL_EXECUTOR);
    }

    public static <T> void threadPoolExecute(DownloadCallback<T> callback) {
        new DownloadTask(callback).executeOnExecutor(DownloadTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    protected final void onPreExecute() {
        super.onPreExecute();
        onStart();
        callback.onStart();
    }

    @Override
    protected final Pair<Exception, T> doInBackground(Object... params) {
        try {
            return new Pair<>(null, call());
        } catch (Exception e) {
            return new Pair<>(e, null);
        }
    }

    @Override
    protected final void onProgressUpdate(Float... values) {
        super.onProgressUpdate(values);
        final float progress = values[0];

        onProgress(progress);
        callback.onProgress(progress);
    }

    @Override
    protected final void onCancelled(Pair<Exception, T> result) {
        finish(result);
    }

    @Override
    protected final void onPostExecute(Pair<Exception, T> result) {
        super.onPostExecute(result);
        finish(result);
    }

    private final void finish(Pair<Exception, T> result) {
        if (result.first != null) {
            onError(result.first);
            callback.onError(result.first);
        } else if (result.second != null) {
            onSuccess(result.second);
            callback.onSuccess(result.second);
        }

        onFinish();
        callback.onFinish();
    }

    @Override
    public void onSuccess(T result) {
    }

    @Override
    public void onError(Exception error) {
    }

    @Override
    public void onFinish() {
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onProgress(float progress) {
    }

    @Override
    public T call() throws Exception {
        return callback.call();
    }
}
