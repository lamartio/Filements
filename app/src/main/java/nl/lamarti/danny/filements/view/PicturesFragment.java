package nl.lamarti.danny.filements.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import nl.lamarti.danny.filements.model.Data;
import nl.lamarti.danny.filements.model.Picture;
import nl.lamarti.danny.filements.model.Pictures;
import nl.lamarti.danny.filements.util.ObservableList;
import nl.lamarti.danny.filements.view.PictureViewHolder.PictureClickListener;

/**
 * Created by DLamarti on 13-2-2016.
 */
public class PicturesFragment extends ListFragment {


    private final DisplayImageOptions displayOptions = new DisplayImageOptions.Builder().displayer(new RoundedBitmapDisplayer(99999)).build();
    private final Runnable observer = new Runnable() {
        @Override
        public void run() {
            final PicturesAdapter adapter = getListAdapter();

            if (adapter != null) {
                final ObservableList<Picture> content = getPictures().getAll();

                adapter.clear();
                adapter.addAll(content);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ListAdapter adapter = new PicturesAdapter(getActivity(), getActivity().getLayoutInflater(), displayOptions, getParent());

        setListAdapter(adapter);
        getPictures().getAll().register(observer);
        observer.run();
    }

    public interface PicturesFragmentParent extends PictureClickListener {

    }

    private PicturesFragmentParent parent;

    private PicturesFragmentParent getParent() {
        if(parent == null) {
            final Fragment pf = getParentFragment();

            if(pf instanceof  PicturesFragmentParent)
                parent = (PicturesFragmentParent) pf;
            else
                parent = (PicturesFragmentParent) getActivity();
        }

        return parent;
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setBackgroundResource(android.R.color.white);
        setEmptyText("There a no pictures downloaded yet. Try the refresh button to download them.");
    }

    @Override
    public void onDestroyView() {
        final ListView view = getListView();
        final int count = view.getChildCount();

        for (int i = 0; i < count; i++) {
            final View child = view.getChildAt(i);
            final PictureViewHolder viewHolder = (PictureViewHolder) child.getTag();

            viewHolder.unregister();
        }

        getPictures().getAll().unregister(observer);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getPictures().getAll().unregister(observer);
    }

    @Override
    public PicturesAdapter getListAdapter() {
        return (PicturesAdapter) super.getListAdapter();
    }

    private Pictures getPictures() {
        return Data.getInstance().getPictures();
    }

}
