package nl.lamarti.danny.filements;

import android.app.Application;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import nl.lamarti.danny.filements.model.Data;

/**
 * Created by DLamarti on 13-2-2016.
 */
public class App extends Application { // uncaught exception handler variable

    @Override
    public void onCreate() {
        super.onCreate();
        final DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        final ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();

        ImageLoader.getInstance().init(config);
        Data.initInstance(this);
    }
}
