package nl.lamarti.danny.filements.model;

import android.graphics.Bitmap;
import android.view.View;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import nl.lamarti.danny.filements.util.DownloadCallback;

/**
 * Created by DLamarti on 13-2-2016.
 */
public class PicturesDownloadCallback implements DownloadCallback<List<Picture>> {

    private final Pictures pictures;

    public PicturesDownloadCallback(Pictures pictures) {
        this.pictures = pictures;
    }

    @Override
    public void onSuccess(List<Picture> result) {
        final Collection<Picture> existingPictures = new ArrayList<>();
        final Collection<Picture> newPictures = new ArrayList<>();

        for (Picture downloadedPicture : result) {
            boolean exists = false;

            for (Picture existingPicture : pictures.all) {
                if (downloadedPicture.hasEqualData(existingPicture)) {
                    existingPictures.add(existingPicture);
                    exists = true;
                    break;
                }
            }

            if (!exists)
                newPictures.add(downloadedPicture);
        }

        pictures.all.retainAll(existingPictures);
        pictures.all.addAll(newPictures);
        pictures.all.notifyObservers();
        pictures.saveQuietly();
    }

    @Override
    public void onError(Exception error) {
    }

    @Override
    public void onFinish() {
        pictures.isDownloading = false;

        for (Picture picture : pictures.all)
            ImageLoader.getInstance().loadImage(picture.url, new SimpleImageLoadingListener(){
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    super.onLoadingComplete(imageUri, view, loadedImage);
                }
            });
    }

    @Override
    public void onStart() {
        pictures.isDownloading = true;
    }

    @Override
    public void onProgress(float progress) {
    }

    @Override
    public List<Picture> call() throws Exception {
        return pictures.parse();
    }
}
