package nl.lamarti.danny.filements.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.view.ViewPager.PageTransformer;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ToxicBakery.viewpager.transforms.RotateDownTransformer;

import nl.lamarti.danny.filements.R;
import nl.lamarti.danny.filements.model.Data;
import nl.lamarti.danny.filements.model.Pictures;

/**
 * Created by DLamarti on 13-2-2016.
 */
public class PictureDetailsFragment extends Fragment {

    private ViewPager pager;
    private View empty;
    private final Runnable selectedObserver = new Runnable() {
        @Override
        public void run() {
            final int index = Data.getInstance().getPictures().getSelectedIndex();

            if (pager != null && index != -1)
                pager.setCurrentItem(index, true);
        }
    };
    private final Runnable contentObserver = new Runnable() {
        @Override
        public void run() {
            final Pictures pictures = Data.getInstance().getPictures();

            if (pictures.getAll().isEmpty()) {
                empty.setVisibility(View.VISIBLE);
                pager.setVisibility(View.GONE);
            } else {
                empty.setVisibility(View.GONE);
                pager.setVisibility(View.VISIBLE);

                if (pager != null && pager.getAdapter() != null)
                    pager.getAdapter().notifyDataSetChanged();
            }
        }
    };
    private final OnPageChangeListener pageChangeListener = new SimpleOnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            final Pictures pictures = Data.getInstance().getPictures();

            pictures.unregisterSelectedChangedObserver(selectedObserver);
            pictures.getAll().get(position).setSelected();
            pictures.registerSelectedChangedObserver(selectedObserver);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.picture_details, container, false);
        final PictureDetailsAdapter adapter = new PictureDetailsAdapter(inflater);
        final PageTransformer transformer = new RotateDownTransformer();

        empty = view.findViewById(R.id.empty);
        pager = (ViewPager) view.findViewById(R.id.viewPager);
        pager.setAdapter(adapter);
        pager.setPageTransformer(true, transformer);
        pager.addOnPageChangeListener(pageChangeListener);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Pictures pictures = Data.getInstance().getPictures();

        pictures.registerSelectedChangedObserver(selectedObserver);
        pictures.getAll().register(contentObserver);
        contentObserver.run();
    }

    @Override
    public void onDestroyView() {
        final Pictures pictures = Data.getInstance().getPictures();

        pictures.unregisterSelectedChangedObserver(selectedObserver);
        pictures.getAll().unregister(contentObserver);
        super.onDestroyView();
    }

}
