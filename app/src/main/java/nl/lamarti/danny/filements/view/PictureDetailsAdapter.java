package nl.lamarti.danny.filements.view;

import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import nl.lamarti.danny.filements.R;
import nl.lamarti.danny.filements.model.Data;
import nl.lamarti.danny.filements.model.Picture;
import nl.lamarti.danny.filements.util.ObservableList;
import uk.co.senab.photoview.PhotoView;

/**
 * Created by DLamarti on 14-2-2016.
 */
public class PictureDetailsAdapter extends PagerAdapter {

    private final LayoutInflater inflater;

    PictureDetailsAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        final ObservableList<Picture> pictures = Data.getInstance().getPictures().getAll();
        final String title = pictures.get(position).title;

        return title;
    }

    @Override
    public ViewHolder instantiateItem(ViewGroup container, int position) {
        final ObservableList<Picture> pictures = Data.getInstance().getPictures().getAll();
        final Picture picture = pictures.get(position);
        final View view = inflater.inflate(R.layout.picture_detail, container, false);
        final ViewHolder holder = new ViewHolder(position, picture, view);

        container.addView(view);
        return holder;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        final ViewHolder holder = (ViewHolder) object;
        final View view = holder.view;

        holder.unregister();
        container.removeView(view);
    }

    @Override
    public int getCount() {
        final ObservableList<Picture> pictures = Data.getInstance().getPictures().getAll();
        final int size = pictures.size();

        return size;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        final ViewHolder holder = (ViewHolder) object;

        return view == holder.view;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    private final static class ViewHolder extends SimpleImageLoadingListener implements Runnable {

        private final int position;
        private final Picture picture;
        private final View view;
        private final View empty;
        private final PhotoView photoView;
        private final TextView title;
        private final TextView description;

        public ViewHolder(int position, Picture picture, View view) {
            this.position = position;
            this.picture = picture;
            this.view = view;
            this.empty = view.findViewById(R.id.empty);
            this.photoView = (PhotoView) view.findViewById(R.id.picture);
            this.title = (TextView) view.findViewById(R.id.title);
            this.description = (TextView) view.findViewById(R.id.desc);

            picture.register(this);
            run();
        }

        @Override
        public void run() {
            final Spanned title = Html.fromHtml(picture.title);
            final Spanned description = Html.fromHtml(picture.description);

            ImageLoader.getInstance().cancelDisplayTask(photoView);
            ImageLoader.getInstance().displayImage(picture.url, photoView, this);

            this.title.setText(title);
            this.description.setText(description);
        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            super.onLoadingComplete(imageUri, view, loadedImage);
            if (loadedImage != null) {
                empty.setVisibility(View.GONE);
                photoView.setVisibility(View.VISIBLE);
            } else {
                empty.setVisibility(View.VISIBLE);
                photoView.setVisibility(View.GONE);
            }
        }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            super.onLoadingFailed(imageUri, view, failReason);
            empty.setVisibility(View.VISIBLE);
            photoView.setVisibility(View.GONE);
        }

        public void unregister() {
            picture.unregister(this);
        }
    }
}
