package nl.lamarti.danny.filements.util;

import java.util.concurrent.Callable;

/**
 * Created by DLamarti on 13-2-2016.
 */
public interface DownloadCallback<T> extends Callable<T> {

    DownloadCallback EMPTY_INSTANCE = new DownloadCallback() {

        @Override
        public Object call() throws Exception {
            return null;
        }

        @Override
        public void onSuccess(Object result) {
        }

        @Override
        public void onError(Exception error) {
        }

        @Override
        public void onFinish() {
        }

        @Override
        public void onStart() {
        }

        @Override
        public void onProgress(float progress) {
        }

    };

    void onSuccess(T result);

    void onError(Exception error);

    void onFinish();

    void onStart();

    void onProgress(float progress);
}