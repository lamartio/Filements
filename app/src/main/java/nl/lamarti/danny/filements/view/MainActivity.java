package nl.lamarti.danny.filements.view;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;

import nl.lamarti.danny.filements.R;
import nl.lamarti.danny.filements.model.Data;
import nl.lamarti.danny.filements.model.Picture;
import nl.lamarti.danny.filements.view.PicturesFragment.PicturesFragmentParent;

/**
 * Created by DLamarti on 13-2-2016.
 */
public class MainActivity extends AppCompatActivity implements PicturesFragmentParent {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer);
        drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, android.R.string.ok, android.R.string.cancel);

        toolbar.setTitleTextColor(ContextCompat.getColor(this, android.R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        drawerToggle.syncState();

        if (savedInstanceState == null) {
            Data.getInstance().getPictures().download();
            drawer.openDrawer(Gravity.LEFT);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (drawer.isDrawerOpen(Gravity.LEFT))
                    drawer.closeDrawers();
                else
                    drawer.openDrawer(Gravity.LEFT);
                return true;
            case R.id.refresh:
                Data.getInstance().getPictures().download();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onPictureClicked(Picture picture) {
        if (drawer != null)
            drawer.closeDrawers();
    }
}
