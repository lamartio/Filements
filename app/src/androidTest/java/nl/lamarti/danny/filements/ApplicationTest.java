package nl.lamarti.danny.filements;

import android.test.ApplicationTestCase;
import android.util.Log;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import nl.lamarti.danny.filements.model.Picture;
import nl.lamarti.danny.filements.model.Pictures;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<App> {

    private final static String TAG = ApplicationTest.class.getSimpleName();

    public ApplicationTest() {
        super(App.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
//        logStream();
//        logParser();
        logParsing();
    }

    public void logStream() {
        try {
            final InputStream input = Pictures.getInputStream();
            final String result = IOUtils.toString(input);

            Log.i(TAG, result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void logParser() {
        try {
            final CSVParser parser = Pictures.getParser();
            final Iterator<CSVRecord> iterator = parser.iterator();

            while (iterator.hasNext()) {
                final CSVRecord record = iterator.next();

                Log.i(TAG, record.toString());
            }

            parser.close();
        } catch (Exception e) {
            Log.e(TAG, ExceptionUtils.getMessage(e), e);
        }
    }

    public void logParsing() {
        try {
            final List<Picture> result = Pictures.parse();

            for(Picture picture : result)
                Log.i(TAG, picture.title + ", " + picture.description + ", " + picture.url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}